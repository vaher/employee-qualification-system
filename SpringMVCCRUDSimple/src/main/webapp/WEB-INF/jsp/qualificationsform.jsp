<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Qualification</h1>
       <form:form method="post" action="save">  
      	<table >
      	 <tr>  
          <td>Employee :</td>  
          <td><form:input path="employee_id" /></td>
         </tr>   
         <tr>  
          <td>Qualification : </td> 
          <td><form:input path="employeeQualifications"  />
         
<head>  
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.dropbtn {
  background-color: #2e512f;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;}
.dropdown {
  position: relative;
  display: inline-block;}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;}
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}
.dropdown-content a:hover {background-color: #ddd;}
.dropdown:hover .dropdown-content {display: block;}
.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
</head>
<body>
<div class="dropdown">
  <button class="dropbtn">Qualification</button>
  <div class="dropdown-content">
    <a href="editqualifications/${qualifications.id}">Java</a>
    <a href="editqualifications/${qualifications.id}">Excel</a>
    <a href="editqualifications/${qualifications.id}">Linux</a>
  </div>
</div>
</body>

</td>
         </tr>  
         <tr>  
          <td>Experience level :</td>  
          <td><form:input path="levels" /></td>
         </tr> 
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Save" /></td>  
         </tr>  
        </table>  
       </form:form>  
