    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Trainings and Exams List</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>ID</th><th>Employee ID</th><th>Qualifications ID</th><th>Level</th></tr>
    <c:forEach var="levels" items="${list}"> 
    <tr>
    <td>${levels.employee_id}</td>
    <td>${levels.firstName}</td>
    <td>${levels.qualification}</td>
    <td>${levels.level}</td>
    <td><a href="edittrainings/${trainings.id}">Edit</a></td>
    <td><a href="deletetrainings/${trainings.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="trainingsform">Add New Training or Exam</a>