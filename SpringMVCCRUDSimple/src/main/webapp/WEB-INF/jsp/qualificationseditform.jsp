<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 

		<h1>Edit Qualifications</h1>
       <form:form method="POST" action="/SpringMVCCRUDSimple/qualification/editsave">  
      	<table >  
      	<tr>
      	<td></td>  
         <td><form:hidden  path="id" /></td>
         </tr> 
          <tr>  
          <td>Employee :</td>  
          <td><form:input path="employee_id" /></td>
         </tr> 
         
         <tr>  
          <td>Qualification : </td> 
   		 <td width=55%> 
          <form:select style="width:152px" path="employeeQualifications">
		  	<form:option value="Java" label="Java"/>
		  	<form:option value="Excel" label="Excel"/>
		  	<form:option value="Word" label="CSS"/>
		   </form:select>
		   </td>		 
		</tr>
		
         <tr>  
          <td>Experience level :</td> 
			<td width=55%> 
          <form:select style="width:152px" path="levels">
		  	<form:option value="Zero" label="Zero Experience"/>
		  	<form:option value="Beginner" label="Beginner"/>
		  	<form:option value="Intermediate" label="Intermediate"/>
		  	<form:option value="Advanced" label="Advanced"/>
		  	<form:option value="Expert" label="Expert"/>
		   </form:select>
		   </td>		 
		</tr>
		
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Edit Save" /></td>  
         </tr>  
        </table>  

		
       </form:form>  
