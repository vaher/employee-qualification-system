    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Trainings and Exams List</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>Trainings and Exams</th><th>Is Training or Exam Completed?</th><th>Edit</th><th>Delete</th></tr>
    <c:forEach var="trainings" items="${list}"> 
    <tr>
    <td>${trainings.id}</td>
    <td>${trainings.trainings_and_exams}</td>
    <td>${trainings.is_completed}</td>
    <td><a href="edittrainings/${trainings.id}">Edit</a></td>
    <td><a href="deletetrainings/${trainings.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="trainingsform">Add New Training or Exam</a>