    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
    

	<h1>Employees List</h1>
	
	<form class="example" action="/SpringMVCCRUDSimple/employee/search">
	  <input type="text" placeholder="Search.." name="search2" >
	  	  	  
	  <td>Sort by :</td> 
	  <select name="orderby" style="width:120px">
	    <option value="lastName">Name</option>
		<option value="date">Adding Time</option>			 
	  </select>
    
      <td>Show :</td> 
	  <select name="count" style="width:120px">
	   <option value="10">10</option>
	   <option value="20">20</option>
	   <option value="20">All</option>			 
      </select>
    	  
      <button type="submit">Submit</button>	  
	  
	</form>
	
	
    
  	
	<table border="2" width="90%" cellpadding="2">
	<tr><br><th>Id</th><th>First name</th><th>Last name</th><th>Personal ID</th><th>Designation</th>
	<th>Qualification</th><th>Level</th><th>Archived</th><th>Adding Time</th><th>Edit</th><th>Delete</th></tr>
    <c:forEach var="employee" items="${list}"> 
    <tr>
    <td>${employee.id}</td>
    <td>${employee.firstName}</td>
    <td>${employee.lastName}</td>
    <td>${employee.personalId}</td>
    <td>${employee.designation}</td>
    <td>${employee.qualification}</td>
    <td>${employee.level}</td>
    <td>${employee.archived}</td>
    <td>${employee.date}</td>
    <td><a href="editemployee/${employee.id}">Edit</a></td>
    <td><a href="deleteemployee/${employee.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="employeeform">Add New Employee</a>