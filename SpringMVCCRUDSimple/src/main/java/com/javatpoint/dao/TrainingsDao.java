package com.javatpoint.dao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.javatpoint.beans.Trainings;  

public class TrainingsDao {
	JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}
	public int save(Trainings p){  
	    String sql="insert into qualifications.trainings(trainings_and_exams,is_completed) values('"+p.getTrainings_and_exams()+"','"+p.getIs_completed()+"')";  
	    return template.update(sql);  
	}  
	public int update(Trainings p){  
	    String sql="update trainings set trainings_and_exams='"+p.getTrainings_and_exams()+"', is_completed='"+p.getIs_completed()+"'where id="+p.getId()+"";  
	    return template.update(sql);  
	}  
	public int delete(int id){  
	    String sql="delete from trainings where id="+id+"";  
	    return template.update(sql);  
	}  
	public Trainings getTrainingsById(int id){  
	    String sql="select * from trainings where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Trainings>(Trainings.class));  
	}
	public List<Trainings> getTrainings(){  
	    return template.query("select * from trainings",new RowMapper<Trainings>(){  
	        public Trainings mapRow(ResultSet rs, int row) throws SQLException {  
	            Trainings t=new Trainings();  
	            t.setId(rs.getInt(1));  
	            t.setTrainings_and_exams(rs.getString(2));  
	            t.setIs_completed(rs.getInt(3));  
	            return t;  
	        }  
	    });  
	}  	
}
