package com.javatpoint.dao;
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
import com.javatpoint.beans.Employee;
 
  
public class EmployeeDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
public int save(Employee p){  
    String sql="insert into Employee(firstName,lastName,personalId,designation,archived,date) "
    		+ "values('"+p.getFirstName()+"','"+p.getLastName()+"','"+p.getPersonalId()+"','"+p.getDesignation()+"',"+p.getArchived()+",'"+dateTime()+"' "+ ")";  
    return template.update(sql);  
}  
public int update(Employee p){  
    String sql="update Employee set firstName='"+p.getFirstName()+"', lastName='"+p.getLastName()+"', designation='"+p.getDesignation()+"', archived="+p.getArchived()+", date='"+p.getDate()+"'"+" where id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="delete from Employee where id="+id+"";  
    return template.update(sql);  
}  
public Employee getEmployeeById(int id){  
    String sql="select * from Employee where id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Employee>(Employee.class));  
}  

public List<Employee> getEmployees(){  
    return template.query("SELECT \r\n" + 
    		"e.id,\r\n" + 
    		" e.firstName,\r\n" + 
    		" e.lastName,\r\n" + 
    		" e.personalId,\r\n" + 
    		"e.designation,\r\n" + 
    		"q.employeeQualifications,\r\n" + 
    		"eq.level,\r\n" + 
    		"e.archived,\r\n" + 
    		"e.date\r\n" + 
    		"FROM qualifications.employee AS e\r\n" + 
    		"JOIN qualifications.employees_qualifications AS eq\r\n" + 
    		"ON e.id = eq.employee_id\r\n" + 
    		"JOIN qualifications.qualifications_test AS q\r\n" + 
    		"ON eq.qualifications_id = q.id",new RowMapper<Employee>(){  
        public Employee mapRow(ResultSet rs, int row) throws SQLException {  
            Employee e=new Employee();  
            e.setId(rs.getInt(1));  
            e.setFirstName(rs.getString(2));  
            e.setLastName(rs.getString(3));   
            e.setPersonalId(rs.getString(4));  
            e.setDesignation(rs.getString(5)); 
            e.setQualification(rs.getString(6));
            e.setLevel(rs.getInt(7));
            e.setArchived(rs.getInt(8));
            e.setDate(rs.getString(9));
            return e;  
        }  
    });  
}  

private String dateTime() {
	DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
	Calendar calendar = Calendar.getInstance();
	Date date = calendar.getTime();

	return dateFormat.format(date);
}


public List<Employee> search(int count, String orderby, String search2) {
	return template.query("SELECT \r\n" + 
			"e.id,\r\n" + 
			" e.firstName,\r\n" + 
			" e.lastName,\r\n" + 
			" e.personalId,\r\n" + 
			"e.designation,\r\n" + 
			"q.employeeQualifications,\r\n" + 
			"eq.level,\r\n" + 
			"e.archived,\r\n" + 
			"e.date\r\n" + 
			"FROM qualifications.employee AS e\r\n" + 
			"JOIN qualifications.employees_qualifications AS eq\r\n" + 
			"ON e.id = eq.employee_id\r\n" + 
			"JOIN qualifications.qualifications_test AS q\r\n" + 
			"ON eq.qualifications_id = q.id where lower(firstName) like '%"+ search2.toLowerCase() 
			+"%' ORDER BY " + orderby + " LIMIT " + count ,new RowMapper<Employee>(){
	        public Employee mapRow(ResultSet rs, int row) throws SQLException {  
	            Employee e=new Employee();  
	            e.setId(rs.getInt(1));  
	            e.setFirstName(rs.getString(2));  
	            e.setLastName(rs.getString(3));   
	            e.setPersonalId(rs.getString(4));  
	            e.setDesignation(rs.getString(5)); 
	            e.setQualification(rs.getString(6));
	            e.setLevel(rs.getInt(7));
	            e.setArchived(rs.getInt(8));
	            e.setDate(rs.getString(9));
	            return e;  
	        }  
	    });  
	
}

}  