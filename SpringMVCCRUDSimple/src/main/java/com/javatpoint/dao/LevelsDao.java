package com.javatpoint.dao;
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
import com.javatpoint.beans.Employee;  
import com.javatpoint.beans.Levels;
  
public class LevelsDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
public int save(Employee p){  
    String sql="insert into Employee(firstName,lastName,personalId,designation) "
    		+ "values('"+p.getFirstName()+"','"+p.getLastName()+"','"+p.getPersonalId()+"','"+p.getDesignation()+"')";  
    return template.update(sql);  
}  
public int update(Employee p){  
    String sql="update Employee set firstName='"+p.getFirstName()+"', lastName='"+p.getLastName()+"', personalId='"+p.getPersonalId()+"', designation='"+p.getDesignation()+"' where id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="delete from Employee where id="+id+"";  
    return template.update(sql);  
}  
public Levels getEmployeeById(int id){  
    String sql="SELECT \r\n" + 
    		"	e.firstName 'first name', \r\n" + 
    		"    e.lastName 'last name', \r\n" + 
    		"    q.employeeQualifications 'skills', \r\n" + 
    		"    eq.level 'level'\r\n" + 
    		"FROM qualifications.employee AS e\r\n" + 
    		"JOIN qualifications.employees_qualifications AS eq\r\n" + 
    		"ON e.id = eq.employee_id\r\n" + 
    		"JOIN qualifications.qualifications_test AS q\r\n" + 
    		"ON eq.qualifications_id = q.id where id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Levels>(Levels.class));  
}  
public List<Levels> getLevels(){  
	String sort = "ASC";
    return template.query("SELECT \r\n" + 
    		"	e.id,\r\n" + 
    		"	e.firstName,\r\n" + 
    		"    q.employeeQualifications,\r\n" + 
    		"    eq.level\r\n" + 
    		"FROM qualifications.employee AS e\r\n" + 
    		"JOIN qualifications.employees_qualifications AS eq\r\n" + 
    		"ON e.id = eq.employee_id\r\n" + 
    		"JOIN qualifications.qualifications_test AS q\r\n" + 
    		"ON eq.qualifications_id = q.id\r\n" + 
    		"WHERE q.employeeQualifications = 'Java'\r\n" + 
    		"ORDER BY eq.level " + sort,new RowMapper<Levels>(){  
        public Levels mapRow(ResultSet rs, int row) throws SQLException {  
            Levels l=new Levels();  
            l.setEmployee_id(rs.getInt(1));  
            l.setFirstName(rs.getString(2));
            l.setQualification(rs.getString(3));   
            l.setLevel(rs.getInt(4));  

            return l;  
        }  
    });  
}  
}  