package com.javatpoint.dao;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;  
import com.javatpoint.beans.Qualifications;  
  
public class QualificationDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
public int save(Qualifications p){  
    String sql="INSERT INTO qualifications_test(employee_id,employeeQualifications,levels) VALUES("+p.getEmployee_id()+",'"+p.getEmployeeQualifications()+"','"+p.getLevels()+"')";  
    return template.update(sql);  
}  
public int update(Qualifications p){  
    String sql="UPDATE qualifications_test SET employee_id="+p.getEmployee_id()+",employeeQualifications='"+p.getEmployeeQualifications()+"', levels='"+p.getLevels()+"' WHERE id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="DELETE FROM qualifications_test WHERE id="+id+"";  
    return template.update(sql);  
}  
public Qualifications getQualificationById(int id){  
    String sql="SELECT * FROM qualifications_test WHERE id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Qualifications>(Qualifications.class));  
}  
public List<Qualifications> getQualifications(){  
    return template.query("SELECT * FROM qualifications_test",new RowMapper<Qualifications>(){  
        public Qualifications mapRow(ResultSet rs, int row) throws SQLException {  
        	Qualifications e=new Qualifications();  
            e.setId(rs.getInt(1));
            e.setEmployee_id(rs.getInt(2));
            e.setEmployeeQualifications(rs.getString(3));  
            e.setLevels(rs.getString(4));  
            return e;  
        }  
    });  
}  
}  