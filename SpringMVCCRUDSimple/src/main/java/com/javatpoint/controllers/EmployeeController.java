package com.javatpoint.controllers;   
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;   
import com.javatpoint.beans.Employee;  
import com.javatpoint.dao.EmployeeDao;

@Controller
@RequestMapping("employee")
public class EmployeeController {  
    
	@Autowired  
    EmployeeDao dao;
    
    @RequestMapping("/employeeform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Employee());
    	return "employeeform"; 
    }  
    
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("employee") Employee employee){  
        dao.save(employee);  
        return "redirect:/employee/viewemployee";
    }  
   
    @RequestMapping("/viewemployee")  
    public String viewemployee(Model m){  
        List<Employee> list=dao.getEmployees();  
        m.addAttribute("list",list);
        return "viewemployee";  
    }  
    
    @RequestMapping(value="/editemployee/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Employee employee=dao.getEmployeeById(id);  
        m.addAttribute("command",employee);
        return "employeeeditform";  
    }  
   
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("employee") Employee employee){  
        dao.update(employee);  
        return "redirect:/employee/viewemployee";  
    }  
      
    @RequestMapping(value="/deleteemployee/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/employee/viewemployee";  
    }   
    
    @RequestMapping(value="/search",method = RequestMethod.GET)  
    public String search(Model m, int count, String orderby, String search2){      
        List<Employee> list=dao.search(count, orderby, search2);  
        m.addAttribute("list",list);
        return "viewemployee";  
    } 
    
   
}  