package com.javatpoint.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.javatpoint.beans.Levels;
import com.javatpoint.beans.Trainings;
import com.javatpoint.dao.LevelsDao;
import com.javatpoint.dao.TrainingsDao;

@Controller  
@RequestMapping("levels")
public class LevelsController {
	
	@Autowired  
    LevelsDao dao;
	
    @RequestMapping("/viewlevels")  
    public String viewlevels(Model m){  
        List<Levels> list=dao.getLevels();  
        m.addAttribute("list",list);
        return "viewlevels";  
    }  
}
