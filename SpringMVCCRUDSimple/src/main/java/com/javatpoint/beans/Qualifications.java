package com.javatpoint.beans;


public class Qualifications{

		private int id;  
		private int employee_id;
		private String employeeQualifications;  
		private String levels;  

		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getEmployeeQualifications() {
			return employeeQualifications;
		}
		public void setEmployeeQualifications(String employeeQualifications) {
			this.employeeQualifications = employeeQualifications;
		}
		public String getLevels() {
			return levels;
		}
		public void setLevels(String levels) {
			this.levels = levels;
		}
		public int getEmployee_id() {
			return employee_id;
		}
		public void setEmployee_id(int employee_id) {
			this.employee_id = employee_id;
		}
}






