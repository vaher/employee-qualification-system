package com.javatpoint.beans;


public class QualificationsType{

		private int type_id;  
		private String employeeQualifications;  

		
		public int getId() {
			return type_id;
		}
		public void setId(int type_id) {
			this.type_id = type_id;
		}
		public String getEmployeeQualifications() {
			return employeeQualifications;
		}
		public void setEmployeeQualifications(String employeeQualifications) {
			this.employeeQualifications = employeeQualifications;
		}
}






