<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  



<!-- Modal content-->
      
    <div class="modal-header">
      <h2 class="modal-title">Edit Task</h2>
    </div>
    <div class="modal-body">


		
       <form:form method="POST" action="/account/employee/editsaveemployeetask"> 
	      	<table >  
		      	 <tr>
			      	<td></td>  
			        <td><form:hidden  path="id" /></td>
		         </tr> 
		         <tr>  
					<td>Description : </td> 
					<td><form:input path="description"  placeholder="Enter description here" /></td>
		         </tr>  
		         
		         <tr>
					<td>Due date : </td>  
					<td><form:input  path="duedate" pattern="[0-9]{4}-([0][1-9]|[1][0-2])-(0[1-9]|[12][0-9]|3[01])" title="yyyy-MM-dd" placeholder="yyyy-MM-dd" /></td>
		         </tr> 
		         <tr>
		      		<td></td>  
		        	<td><form:hidden  path="employeeId" /></td>
		         </tr>
		
		         <tr>  
		         	<td>Completed :</td> 
				 	<td><form:checkbox path="done" /></td>
				 </tr> 
	         
	        </table>  
	        
	        <br>
	        
	        <tr>  
	          <td> </td>  
	          <td><input type="submit" value="Save" /></td>  
	         </tr>  
	        
        
       </form:form>  
       
       
   	</div>
	<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div>
             
