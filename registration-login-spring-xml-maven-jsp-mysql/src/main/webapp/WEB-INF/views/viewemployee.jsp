    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
	 
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>



    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Welcome</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bootstrap CRUD Data Table for Database with Modal Form</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">

	
  body {
        color: #566787;
		background: #f5f5f5;
		font-family: 'Roboto Expanded', sans-serif;
		font-size: 15px;
		
	}

	</style>
	<script type="text/javascript">
	$(document).ready(function(){
		// Activate tooltip
		$('[data-toggle="tooltip"]').tooltip();
		
		// Select/Deselect checkboxes
		var checkbox = $('table tbody input[type="checkbox"]');
		$("#selectAll").click(function(){
			if(this.checked){
				checkbox.each(function(){
					this.checked = true;                        
				});
			} else{
				checkbox.each(function(){
					this.checked = false;                        
				});
			} 
		});
		checkbox.click(function(){
			if(!this.checked){
				$("#selectAll").prop("checked", false);
			}
		});
	});
	</script>
	    
    
    
    
    
</head>
<body>

		
		<nav class="navbar navbar-custom">
		  <div class="container">
		    
		    <ul class="nav navbar-nav">
		      <li class="active"><a href="../employee/viewemployee">Home</a></li>
		      
		         <li><a href="../employee/myinfo">My Info</a></li>
		      <li><a href="../qualification/myqualifications">My Qualifications</a></li>
		      <li><a href="../tasks/viewtasks">My Tasks</a></li>
		    
		    </ul>
		    <ul class="nav navbar-nav navbar-right">
		      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Signed in as ${pageContext.request.userPrincipal.name} <span class="caret"></span></a>
		        <ul class="dropdown-menu">		                   
		          <li><a onclick="document.forms['logoutForm'].submit()">Logout</a></li>
		        </ul>
		      </li>
		    </ul>
		  </div>
		</nav>
		
			
		
		<div class="container">



    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>

       
       

    </c:if>
    

</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

    



    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>Employees <b>List</b></h2>
					</div>
					<div class="col-sm-6">
						
						<c:if test="${isAdmin}"><a href="employeeform" class="btn btn-success" data-toggle="modal" data-target="#myModal"><i class="material-icons">&#xE147;</i> <span>Add New Employee</span></a></c:if>
							
					</div>
                </div>
            </div>
            
            
            <form class="example" action="/account/employee/search">
			  <input type="text" class="form-control" placeholder="Search.." name="search2" >
			  	  	  
			  <select name="orderby" class="form-control-two">
			    <option value="id" selected>Sort by:</option>
			  	<option value="firstName">First Name</option>
			    <option value="lastName">Last Name</option>
				<option value="date">Adding Time</option>	
				<option value="employeeQualifications">Qualification</option>		 
			  </select>
		           
			  <select name="count" class="form-control-two">
			   <option value="100" selected>Show:</option>
			   <option value="10">10</option>
			   <option value="20">20</option>
			   <option value="100">All</option>			 
		      </select>
		    	  
		      <button type="submit" class="btn btn-dark" >Submit</button>	  
			  
			</form>
            
            
            <table class="table table-striped table-hover" >
                                  
				<tr><br><th>Id</th><th>First name</th><th>Last name</th><th>Personal ID</th><th>Designation</th>
			
				<th>Qualification</th><th>Level</th><c:if test="${isAdmin}"><th>Archived</th></c:if><th>Adding Time</th><th>Actions</th><c:if test="${isAdmin}"></c:if></tr>
			
			    <c:forEach var="employee" items="${list}"> 
			    <tr>
				    <td>${employee.id}</td>
				    <td>${employee.firstName}</td>
				    <td>${employee.lastName}</td>
				    <td>${employee.personalId}</td>
				    <td>${employee.designation}</td>
				    <td>${employee.qualification}</td>
				    <td>${employee.level}</td>
				    <c:if test="${isAdmin}"><td>${employee.archived}</td></c:if>
				    <td>${employee.date}</td>
				    
				  
				   
			    	
			    	<td>
			    	   <a href="employeeview/${employee.id}" class="view"><i class="material-icons" data-toggle="tooltip" title="View">&#xE417;</i></a>
			    	   <a href="editemployee/${employee.id}" class="edit"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                       <c:if test="${isAdmin}"><a href="deleteemployee/${employee.id}" class="delete"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a></c:if>
                             
                    </td>
			    
			    </tr>
			    </c:forEach>
			    		    			    
            </table>
        </div>
    </div>
    
    
      <!-- Modal -->

		<div class="container">
		  				
		  <div class="modal fade" id="myModal" role="dialog" style="layout:center">
		    <div class="modal-dialog">
		    
		      <!-- Modal content-->
		      <div class="modal-content">
		        <div class="modal-header">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 class="modal-title">Modal Header</h4>
		        </div>
		        <div class="modal-body">
		          <p>Some text in the modal.</p>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
		      </div>
		      
		    </div>
		  </div>
		  
		</div>
	
    
	
</body>
</html>





                                		                            