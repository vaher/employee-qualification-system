<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  


 <!-- Modal content-->
      
    <div class="modal-header">
      <h2 class="modal-title">Edit Level</h2>
    </div>
    <div class="modal-body">



      <form:form method="POST" action="/account/employee/editsavequalification">  
	     	<table >  
	     	<tr>
	        <td><form:hidden  path="id" /></td>
	        </tr> 
	        
	        <tr>   
	          <td><form:hidden path="employeeId" /></td>
	         </tr> 
	        
	  
	        	<tr>  
	          <td><form:hidden path="qualificationId" /></td>
	         </tr> 
	         
			
	         <tr>  
	          <td>Experience level :</td> 
				<td width=55%> 
	          <form:select style="width:152px" path="levelId">
			  	<form:option value="1" label="N/A"/>
			  	<form:option value="2" label="Beginner"/>
			  	<form:option value="3" label="Intermediate"/>
			  	<form:option value="4" label="Advanced"/>
			  	<form:option value="5" label="Expert"/>
			   </form:select>
			   </td>		 
			</tr>
	         
	        </table> 
	        
	        <br>
	        
	        <tr>  
	          <td> </td>  
	          <td><input type="submit" value="Save" /></td>  
	         </tr> 
       </form:form>  

     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     </div>
      
	       


