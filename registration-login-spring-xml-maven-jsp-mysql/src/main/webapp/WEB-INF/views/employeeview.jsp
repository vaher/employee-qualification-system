<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
 <c:set var="contextPath" value="${pageContext.request.contextPath}"/> 


<!DOCTYPE html>
<html lang="en">
<head>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	
	<title>Welcome</title>
	
	<link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Bootstrap CRUD Data Table for Database with Modal Form</title>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<style type="text/css">
		body {
		    color: #566787;
			background: #f5f5f5;
			font-family: 'Roboto Expanded', sans-serif;
			font-size: 15px;			
		}
	</style>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// Activate tooltip
			$('[data-toggle="tooltip"]').tooltip();
			
		});
		
		
		$(function() {
			$('.pop').on('click', function() {
				$('.imagepreview').attr('src', $(this).find('img').attr('src'));
				$('#imagemodal').modal('show');   
			});		
	    });
		
	</script>

</head>
<body>

	<nav class="navbar navbar-custom">
	  <div class="container" style="width:55%">
	    
	    <ul class="nav navbar-nav">
	      <li class="active"><a href="../../employee/viewemployee">Home</a></li>
	      
	      
	      <li><a href="../../employee/myinfo">My Info</a></li>
		   <li><a href="../../qualification/myqualifications">My Qualifications</a></li>
	      <li><a href="../../tasks/viewtasks">My Tasks</a></li>
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Signed in as ${pageContext.request.userPrincipal.name} <span class="caret"></span></a>
	        <ul class="dropdown-menu">		                   
	          <li><a onclick="document.forms['logoutForm'].submit()">Logout</a></li>
	        </ul>
	      </li>
	    </ul>
	  </div>
	</nav>


    <div class="container" style="width:55%">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
						<h2>View <b>Employee</b></h2>
					</div>
					<div class="col-sm-6">
						<a href="../editemployee/${employee.id}" class="btn btn-warning"><i class="material-icons">&#xE254;</i> <span>Edit Employee</span></a>
					</div>
         		</div>
     		</div>
            
             
	 <c:if test="${pageContext.request.userPrincipal.name != null}">
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
	</c:if>
			

	<h1>${employee.firstName} ${employee.lastName}<a href="#" class="pop"><img align=right width="120"
     				src="../getPhoto/<c:out value='${employee.id}'/>"></a></h1>
     				
     			
	

   
		<table class="table table-striped table-hover">
			<tr><br><br><th>First name</th><th>Last name</th><th>Personal ID</th><th>Designation</th>
				<th>Archived</th><th>Adding Time</th></tr>
		    <tr>
			    <td width=15%>${employee.firstName}</td>
			    <td>${employee.lastName}</td>
			    <td>${employee.personalId}</td>
			    <td>${employee.designation}</td>
			    <td>${employee.archived}</td>
			    <td>${employee.date}</td>
			    
		    </tr>
	    </table>
	    
	    
    
    <table class="table table-striped table-hover">
	   <tr><br><th>Qualification</th><th>Level</th></tr>
	    <c:forEach var="qualification" items="${qualifications}"> 
	    <tr>	      
	    <td width=32%>${qualification.qualificationName}</td>
	    <td>${qualification.levelName}</td> 
	    </tr>
	  </c:forEach>
	</table>
    			
	
        </div>
    </div>
    
    
    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">              
		      <div class="modal-body">
		      	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        <img src="" class="imagepreview" style="width: 100%;" >
		      </div>
		    </div>
		  </div>
		</div>
    
	
	
</body>
</html>



    