<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  


      <!-- Modal content-->
     
        <div class="modal-header">
          
          <h2 class="modal-title">Add New Qualification</h2>
        </div>
        <div class="modal-body">
         

					
	       <form:form method="post" action="../savequalification">  
	      	<table>
	           
	
	        <tr>
		     	<td></td>  
		        <td><form:hidden  path="employeeId" /></td>
	        </tr>  
	           
	           
			<tr>  
	          <td  width=52%>Qualification : </td> 
	   		  <td> 
		          <form:select style="width:170px" path="qualificationId">
				  	<form:option value="1" label="Java"/>
				  	<form:option value="2" label="Python"/>
				  	<form:option value="3" label="C"/>
				  	<form:option value="4" label="C++"/>
				  	<form:option value="5" label="C#"/>
				  	<form:option value="6" label="R"/>
				  	<form:option value="7" label="JavaScript"/>
				  	<form:option value="8" label="GO"/>
				  	<form:option value="9" label="Swift"/>
				  	<form:option value="10" label="Ruby"/>
				  	<form:option value="11" label="PHP"/>
				  	<form:option value="12" label="SQL"/>
				  	<form:option value="13" label="HTML"/>
				  	<form:option value="14" label="CSS"/>
				  			
				   </form:select>
			   </td>		 
			</tr>
			
	        <tr>  
	           <td width=52%>Experience level :</td> 
			   <td> 
		          <form:select style="width:170px" path="levelId">
				  	<form:option value="1" label="N/A"/>
				  	<form:option value="2" label="Beginner"/>
				  	<form:option value="3" label="Intermediate"/>
				  	<form:option value="4" label="Advanced"/>
				  	<form:option value="5" label="Expert"/>
				   </form:select>
			   </td>		 
			</tr>
	                 				         
	    
	        </table> 
	        
	         <tr>  
	          <td> </td>  
	          <td><br><input type="submit" value="Save" /></td>  
	         </tr>   
	       </form:form>  
			



        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    
 
  
				