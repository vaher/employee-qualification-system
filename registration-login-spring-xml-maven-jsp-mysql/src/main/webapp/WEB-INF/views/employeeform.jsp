<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  


  <!-- Modal content-->
      
        <div class="modal-header">
          
          <h2 class="modal-title">Add New Employee</h2>
        </div>
        <div class="modal-body">


       <form:form method="post" action="save?${_csrf.parameterName}=${_csrf.token}" enctype="multipart/form-data" >  
      	<table >  
         <tr>  
          <td>First name : </td> 
          <td><form:input path="firstName" style="text-transform: capitalize;" /></td>
         </tr>  
         <tr>  
          <td>Last name : </td> 
          <td><form:input path="lastName" style="text-transform: capitalize;" /></td>
         </tr>
         <tr>  
          <td>Personal ID : </td> 
          <td><form:input path="personalId" pattern="([3-4]([0-9]{2})|[5-6](0[0-9]|1[1-9]))(1[0-2]|0[1-9])(0[1-9]|[12][0-9]|3[01])[0-9]{4}" title="Eesti isikukood" /></td>
         </tr>
         <tr>  
          <td>Designation :</td>  
          <td><form:input path="designation" /></td>
         </tr> 
   		          
          <tr>  
          <td>User Id :</td>  
          <td><form:select path="userId" items="${users}" /></td>
         </tr> 
         
         <tr>
            <td colspan="2">
            File to upload: <input type="file" name="file">
            </td>
        </tr>
   		 
   	         
        </table>  
         
                 
         <tr>
         <td> </td>
         <td><br><input type="submit" value="Save" /></td>
         </tr> 
         
       </form:form>
       
       
       
         </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
  
        
         
