package com.hellokoding.account.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Tasks {  
	private int id;  
	private String description;
	private boolean done;
	private String duedate;
	private int employeeId;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthday;
	
	  
	public int getId() {  
	    return id;  
	}  
	public void setId(int id) {  
	    this.id = id;  
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	
	public boolean isDone() {
		return done;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	

  
}  