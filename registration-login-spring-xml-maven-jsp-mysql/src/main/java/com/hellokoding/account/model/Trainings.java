package com.hellokoding.account.model; 

public class Trainings {
	
	private int id;
	private String trainings_and_exams;
	private int is_completed;
	
	public int getIs_completed() {
		return is_completed;
	}
	public void setIs_completed(int is_completed) {
		this.is_completed = is_completed;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTrainings_and_exams() {
		return trainings_and_exams;
	}
	public void setTrainings_and_exams(String trainings_and_exams) {
		this.trainings_and_exams = trainings_and_exams;
	}

}
