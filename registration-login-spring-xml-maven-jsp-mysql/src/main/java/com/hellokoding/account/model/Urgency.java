package com.hellokoding.account.model;

public class Urgency {
	private static int id;
	private static String name;
	
	
	public static int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
