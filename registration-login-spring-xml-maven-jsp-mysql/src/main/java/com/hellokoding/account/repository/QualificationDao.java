package com.hellokoding.account.repository;
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.EmployeeQualification;
import com.hellokoding.account.model.Tasks;
  
  
public class QualificationDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
public int save(EmployeeQualification p){  
    String sql="INSERT INTO employee_qualification(employeeId,qualificationId,levelId) VALUES("+p.getEmployeeId()+",'"+p.getQualificationId()+"','"+p.getLevelId()+"')";  
    return template.update(sql);  
}  
public int update(EmployeeQualification p){  
    String sql="UPDATE employee_qualification SET employeeId="+p.getEmployeeId()+",qualificationId='"+p.getQualificationId()+"', levelId='"+p.getLevelId()+"' WHERE id="+p.getId()+"";  
    return template.update(sql);  
}  
public int delete(int id){  
    String sql="DELETE FROM employee_qualification WHERE id="+id+"";  
    return template.update(sql);  
}  
public EmployeeQualification getQualificationById(int id){  
    String sql="SELECT * FROM employee_qualification WHERE id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<EmployeeQualification>(EmployeeQualification.class));  
} 




public List<EmployeeQualification> getQualifications(){  
    return template.query("SELECT * FROM employee_qualification",new RowMapper<EmployeeQualification >(){  
        public EmployeeQualification  mapRow(ResultSet rs, int row) throws SQLException {  
        	EmployeeQualification e=new EmployeeQualification();  
            e.setId(rs.getInt(1));
            e.setEmployeeId(rs.getInt(2));
            e.setQualificationId(rs.getInt(3));  
            e.setLevelId(rs.getInt(4));  
            return e;  
        }  
    });  
}  

public List<EmployeeQualification> getQualificationsByUserId(long userId){  
    return template.query("SELECT eq.* FROM accounts.employee AS e\r\n" + 
    		"JOIN user AS u\r\n" + 
    		"ON e.userId=u.id\r\n" + 
    		"JOIN employee_qualification AS eq\r\n" + 
    		"ON eq.employeeId=e.id\r\n" + 
    		"WHERE userId="+userId, new RowMapper<EmployeeQualification>(){  
        public EmployeeQualification mapRow(ResultSet rs, int row) throws SQLException {  
        	EmployeeQualification e=new EmployeeQualification();  
        	 e.setId(rs.getInt(1));
             e.setEmployeeId(rs.getInt(2));
             e.setQualificationId(rs.getInt(3));  
             e.setLevelId(rs.getInt(4));  
            return e;  
        }  
    });    

}  
}  