package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.Urgency;
import com.hellokoding.account.model.Tasks;  
  
public class TasksDao {  
JdbcTemplate template;  
  
public void setTemplate(JdbcTemplate template) {  
    this.template = template;  
}  
/* public int save(Tasks p){  
    String sql="INSERT INTO Tasks(description,done,duedate,taskownerId) values('"+p.getDescription()+"',"+p.isDone()+","+p.getDuedate()+","+p.getTaskownerId()+")";  
    return template.update(sql);  
}  */
/* public int update(Tasks p){  
    String sql="UPDATE Tasks SET description='"+p.getDescription()+"', done="+p.isDone()+", duedate="+p.getDuedate()+", TaskOwnerID="+p.getTaskownerId()+" WHERE id="+p.getId()+"";  
    return template.update(sql);  
}  */

public int savetask(Tasks p){  
    String sql="INSERT INTO tasks(description,done,duedate,employeeId) values('"+p.getDescription()+"',"+p.isDone()+",'"+p.getDuedate()+"',"+p.getEmployeeId()+")";  
    return template.update(sql);  
} 

public int updatetask(Tasks p){  
String sql="UPDATE tasks SET description='"+p.getDescription()+"', done="+p.isDone()+", duedate='"+p.getDuedate()+"', employeeId="+p.getEmployeeId()+" WHERE id="+p.getId()+"";  
return template.update(sql);  
}  

public int deletetask(int id){  
    String sql="DELETE FROM tasks WHERE id="+id+"";  
    return template.update(sql);  
}  

public Tasks getTasksById(int id){  
    String sql="SELECT * FROM Tasks WHERE id=?";  
    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Tasks>(Tasks.class));  
}  
public List<Tasks> getEmployees(){  
    return template.query("SELECT * FROM tasks",new RowMapper<Tasks>(){  
        public Tasks mapRow(ResultSet rs, int row) throws SQLException {  
        	Tasks e=new Tasks();  
            e.setId(rs.getInt(1));  
            e.setDescription(rs.getString(2));  
            e.setDone(rs.getBoolean(3));
            e.setDuedate(rs.getString(4));
            return e;  
        }  
    });    

}  


public List<Tasks> getTasksByUserId(long userId){  
    return template.query("SELECT t.* FROM accounts.employee AS e\r\n" + 
    		"JOIN user AS u\r\n" + 
    		"ON e.userId=u.id\r\n" + 
    		"JOIN tasks AS t\r\n" + 
    		"ON t.employeeId=e.id\r\n" + 
    		"WHERE userId="+userId,new RowMapper<Tasks>(){  
        public Tasks mapRow(ResultSet rs, int row) throws SQLException {  
        	Tasks e=new Tasks();  
            e.setId(rs.getInt(1));  
            e.setDescription(rs.getString(2));  
            e.setDone(rs.getBoolean(3));
            e.setDuedate(rs.getString(4));
            return e;  
        }  
    });    

}  


//public Tasks getTasksByUserId(long userId){  
//    String sql="SELECT * FROM Tasks WHERE employeeId=?";  
//    return template.queryForObject(sql, new Object[]{userId},new BeanPropertyRowMapper<Tasks>(Tasks.class));  
//}  

}  