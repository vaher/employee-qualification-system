package com.hellokoding.account.web; 

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hellokoding.account.model.Levels;
import com.hellokoding.account.model.Trainings;
import com.hellokoding.account.repository.LevelsDao;
import com.hellokoding.account.repository.TrainingsDao;

@Controller  
@RequestMapping("levels")
public class LevelsController {
	
	@Autowired  
    LevelsDao dao;
	
    @RequestMapping("/viewlevels")  
    public String viewlevels(Model m){  
        List<Levels> list=dao.getLevels();  
        m.addAttribute("list",list);
        return "viewlevels";  
    }  
}
