package com.hellokoding.account.web;   
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;

import com.hellokoding.account.model.Employee;
import com.hellokoding.account.model.EmployeeQualification;
import com.hellokoding.account.model.Tasks;
import com.hellokoding.account.repository.EmployeeDao;
import com.hellokoding.account.repository.QualificationDao;
import com.hellokoding.account.service.UserService;  
@Controller 
@RequestMapping("qualification")
public class QualificationController {  
    @Autowired  
    QualificationDao dao;
    
    @Autowired  
    EmployeeDao employeeDao;
    
    @Autowired
    private UserService userService;
      
    @RequestMapping("/qualificationsform")  
    public String showform(Model m){  
    	m.addAttribute("command", new EmployeeQualification());
    	return "qualificationsform"; 
    }  
 
//    @RequestMapping(value="/save",method = RequestMethod.POST)  
//    public String save(@ModelAttribute("qualifications") Qualifications qualifications){  
//        dao.save(qualifications);  
//        return "redirect:/qualification/viewqualifications"; 
//    }  

    @RequestMapping("/viewqualifications")  
    public String viewqualifications(Model m){  
        List<EmployeeQualification> list=dao.getQualifications();  
        m.addAttribute("list",list);
        return "viewqualifications";  
    }  
  
   @RequestMapping(value="/editqualifications/")  
    public String edit(@PathVariable int id, Model m){  
    	EmployeeQualification qualifications=dao.getQualificationById(id);  
        m.addAttribute("command",qualifications);
        return "qualificationseditform";  
    }  
 
    @RequestMapping(value="/editsavequalification",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("qualifications") EmployeeQualification qualifications){  
      //  dao.update(qualifications);  
        return "redirect:viewqualifications";  
    }  
 
    @RequestMapping(value="/deletequalifications/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/qualification/viewqualifications";  
    }   
    
    @RequestMapping("/myqualifications")  
    public String viewMyQualifications(Model m){ 
    	long userId = userService.getLoggedInUser().getId();
        List<EmployeeQualification> qualifications=dao.getQualificationsByUserId(userId);
        m.addAttribute("qualifications", employeeDao.getQualifications(employeeDao.getEmployeeByUserId(userId).getId()));
        return "myqualifications"; 
    }
}  