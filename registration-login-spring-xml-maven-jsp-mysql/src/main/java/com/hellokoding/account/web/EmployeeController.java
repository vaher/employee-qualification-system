package com.hellokoding.account.web; 
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.hellokoding.account.service.UserService; 

import com.hellokoding.account.model.Employee;

import com.hellokoding.account.model.EmployeeQualification;
import com.hellokoding.account.model.Tasks;
import com.hellokoding.account.model.User;

import com.hellokoding.account.repository.EmployeeDao;
import com.hellokoding.account.repository.QualificationDao;
import com.hellokoding.account.repository.TasksDao;
import com.hellokoding.account.repository.UserRepository;

@Controller
@RequestMapping("employee")
public class EmployeeController {  
    
	@Autowired  
    EmployeeDao dao;
	
    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired  
    QualificationDao daoQualification;  
    

    @Autowired  
    TasksDao daoTasks;
    

    
    @RequestMapping("/employeeform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Employee());
    	m.addAttribute("users", usersToMap(userRepository.findAll()));
    	return "employeeform"; 
    }  
    
    @RequestMapping(value="/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("employee") Employee employee, @RequestParam("file") MultipartFile file){
    	try {
            employee.setPicture(file.getBytes());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        dao.save(employee);  
        return "redirect:/employee/viewemployee";
    }  
    
    @RequestMapping(value = "/getPhoto/{id}")
    public void getPhoto(HttpServletResponse response, @PathVariable("id") int id) throws Exception {
        response.setContentType("image/jpeg");
        
        Employee employee = dao.getEmployeeById(id);
        byte[] bytes = employee.getPicture();
        InputStream inputStream = new ByteArrayInputStream(bytes);
        IOUtils.copy(inputStream, response.getOutputStream());
    } 
    
    
   
    @RequestMapping("/viewemployee")  
    public String viewemployee(Model m){  
    	
    	
    	
    	
        List<Employee> list=dao.getEmployees();  
        m.addAttribute("list",list);
        boolean isAdmin =  userService.hasRole("ROLE_ADMIN");
        m.addAttribute("isAdmin", userService.hasRole("ROLE_ADMIN")); 
        return "viewemployee";  
    }  
    
    @RequestMapping("/employeeview/{id}")  
    public String viewOneEmployee(@PathVariable int id, Model m){  
    	Employee employee=dao.getEmployeeById(id);  
    	m.addAttribute("employee", dao.getEmployeeById(id));
        m.addAttribute("qualifications", dao.getQualifications(id));
        return "employeeview";  
    }
    
    @RequestMapping(value="/editemployee/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Employee employee=dao.getEmployeeById(id);  
        m.addAttribute("command",employee);
        m.addAttribute("qualifications", dao.getQualifications(id));
        m.addAttribute("users", usersToMap(userRepository.findAll()));
        m.addAttribute("tasks", dao.getTasks(id));
        return "employeeeditform";  
    }  
    
    @RequestMapping(value="/editqualification/{id}")  
    public String editQualification(@PathVariable int id, Model m){  
    	m.addAttribute("qualifications", dao.getQualifications(id));
        m.addAttribute("command",dao.getQualificationById(id));
        return "editqualification";  
    }  
   
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("employee") Employee employee){  
        dao.update(employee);  
        return "redirect:/employee/viewemployee";  
    }  
      
    @RequestMapping(value="/deleteemployee/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/employee/viewemployee";  
    }   
    
    @RequestMapping(value="/deletequalification/{id}",method = RequestMethod.GET)  
    public String deleteQualification(@PathVariable int id){ 
    	int employeeId = dao.getQualificationById(id).getEmployeeId();
        dao.deleteQualification(id);  
        return "redirect:/employee/editemployee/" + employeeId;  
    }   
    
    @RequestMapping(value="/search",method = RequestMethod.GET)  
    public String search(Model m, int count, String orderby, String search2){      
        List<Employee> list=dao.search(count, orderby, search2);  
        m.addAttribute("list",list);
        return "viewemployee";  
    } 
    

    
    
   @RequestMapping("/qualificationsform/{id}")  
    public String showqualificationform(@PathVariable int id, Model m){  
    	m.addAttribute("qualifications", dao.getQualifications(id));
    	EmployeeQualification employeeQualification  = new EmployeeQualification ();
    	employeeQualification.setEmployeeId(id);
    	
    	m.addAttribute("command", employeeQualification );
    	return "qualificationsform"; 
    } 
    
   
    @RequestMapping(value="/savequalification",method = RequestMethod.POST)  
    public String save(@ModelAttribute("qualifications") EmployeeQualification qualifications){  
        daoQualification.save(qualifications);  
  
        return "redirect:/employee/editemployee/" + qualifications.getEmployeeId();  
    }  
    
    @RequestMapping(value="/savetask",method = RequestMethod.POST)  
    public String save(@ModelAttribute("tasks") Tasks tasks){  
        daoTasks.savetask(tasks);  
        return "redirect:/employee/editemployee/" + tasks.getEmployeeId();
    }  
    
    
    
    /*@RequestMapping(value="/qualificationsform/save",method = RequestMethod.POST)  
    public String save(@ModelAttribute("qualifications") Qualifications qualifications){ 
    	int employeeId = dao.getQualificationById(id).getEmployeeId();
        daoQualification.save(qualifications);  
        return "redirect:/employee/editemployee/" + employeeId;  
    }  */
    
    @RequestMapping(value="/editsavequalification",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("qualification") EmployeeQualification qualifications){  
        daoQualification.update(qualifications);  
        return "redirect:/employee/editemployee/" + qualifications.getEmployeeId(); 
    }
    
    
    private Map<Long, String> usersToMap(List<User> users) {
    	Map<Long, String> usersMap = new HashMap<>();
    	
    	for(User user : users) {
    		usersMap.put(user.getId(),user.getUsername());
    	}
    	
    	return usersMap;
    }
    

    @RequestMapping("/tasksform/{id}")  
    public String showtaskform(@PathVariable int id, Model m){  
    	m.addAttribute("tasks", dao.getTasks(id));
    	Tasks tasks  = new Tasks ();
    	tasks.setEmployeeId(id);
    	
    	m.addAttribute("command", tasks );
    	return "tasksform"; 
    } 
    
    @RequestMapping(value="/editemployeetasks/{id}")  
    public String editTasks(@PathVariable int id, Model m){  
        Tasks tasks=daoTasks.getTasksById(id);  
        m.addAttribute("command",tasks);
        return "employeetaskseditform";  
    }  
    

    
    @RequestMapping(value="/deletetask/{id}",method = RequestMethod.GET)  
    public String deletetask(@PathVariable int id){ 
    	int employeeId = daoTasks.getTasksById(id).getEmployeeId();
        daoTasks.deletetask(id);  
        return "redirect:/employee/editemployee/" + employeeId;  
    }   
    
    
    @RequestMapping(value="/editsaveemployeetask",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("tasks") Tasks tasks){  
    	daoTasks.updatetask(tasks);  
        return "redirect:/employee/editemployee/" + tasks.getEmployeeId();  
    }  
    
    

    @RequestMapping("/myinfo")  
    public String viewMyInfo(Model m){  
        List<Employee> list=dao.getInfoByUserId(userService.getLoggedInUser().getId());
        m.addAttribute("list",list);
        return "myinfo"; 
    }

}  